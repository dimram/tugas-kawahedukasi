package tech.dimas.models;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class Employee {
    @NotBlank(message = "Nama tidak kosong") //pesan error jika kosong
    public String name;
    @NotNull(message = "Nim tidak kosong") //pesan error jika kosong
    public Integer nim;
    @Min(value = 0, message = "UKT tidak boleh kurang dari 0") //pesan error jika 0
    @Max(value = 7500000 message = "UKT tidak boleh lebih dari 0") //pesan error jika 7500000
    public Double salary;
    // @NotBlank(message = "true = sudah bayar || false = belum bayar") //pesan error jika kosong
    public Boolean status;
    
    public Employee() {
    }
    
    public Employee(String name, Integer nim, Double salary, Boolean status) {
        this.name = name;
        this.nim = nim;
        this.salary = salary;
        this.status = status;
}

    public static void main(String[] args) {
    }
    
}
