package revisitugaskedua;
import java.util.Scanner;
public class RevisiTugasKedua {
    public static void main(String[] args) {
        System.out.println(nilaiMutlak(-8));
        int[] arrayApapun = {3, 7, 9, 6, 0, -7};
        System.out.println(pangkat(7,5)); 
        
    }
    public static float nilaiMutlak(float nilaiA){
        if(nilaiA < 0){
            return nilaiA * -1;
        }
        return nilaiA;
    }
    public static int angkaTerbesar(int[] arrayA){
        int angkaTerbesar = arrayA[0];
        
        for(int angka : arrayA){
        
        if(angkaTerbesar < angka){
           angkaTerbesar = angka; 
        }
    }    
        return angkaTerbesar;
    }
    public static float pangkat(float basis, float eksponen){
        float hasil = 1;
        for(float i=0; i < eksponen; i++){
            hasil *= basis;
        }
        return hasil;
    }

}